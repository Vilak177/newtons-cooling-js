function Newtons(){
	
	var eventos = function(){

		$(".calculus_button").off();
		$(".selector_variable").off();

		$(".selector_variable").change(function(){
			var var_select = $(this).val();
			if(var_select == 1){
				fill_fields(2,3,4,5);
			}
			else if(var_select == 2){
				fill_fields(1,3,4,5);
			}
			else if(var_select == 3){
				fill_fields(1,2,4,5);
			}
			else if(var_select == 4){
				fill_fields(1,2,3,5);
			}
			else if(var_select == 5){
				fill_fields(1,2,3,4);
			}
			else if(!var_select){
				$(".container_variables").html("");
			}
			$(".container_resupuesta").html("");
		});

		$(".calculus_button").click(function(){
			var heat_transfer_ratio = $(".heat_transfer_rate").val();
			var heat_transfer_coeff = $(".heat_transfer_coefficient").val();
			var temp = $(".temperature").val();
			var eq_temp = $(".equilibrium_temperature").val();
			var area = $(".area").val();
			if(typeof heat_transfer_ratio == "undefined"){
				if(!isNaN(parseFloat(heat_transfer_coeff))){
					if(!isNaN(parseFloat(temp))){
						if(!isNaN(parseFloat(eq_temp))){
							if(!isNaN(parseFloat(area))){
								calcula_htr(heat_transfer_coeff,temp,eq_temp,area);
							}
							else{
							alert("El área debe ser un número");
							}
						}
						else{
							alert("La temperatura de equilibrio debe ser un número");
						}
					}
					else{
						alert("La temperatura debe ser un número");
					}
				}
				else{
					alert("El coeficiente de transferencia de calor debe ser un número");
				}	
			}
			else if(typeof heat_transfer_coeff == "undefined"){
				if(!isNaN(parseFloat(heat_transfer_ratio))){
					if(!isNaN(parseFloat(temp))){
						if(!isNaN(parseFloat(eq_temp))){
							if(!isNaN(parseFloat(area))){
								calcula_htc(heat_transfer_ratio,temp,eq_temp,area);
							}
							else{
							alert("El área debe ser un número");
							}
						}
						else{
							alert("La temperatura de equilibrio debe ser un número");
						}
					}
					else{
						alert("La temperatura debe ser un número");
					}
				}
				else{
					alert("La tasa de transferencia de calor debe ser un número");
				}	
			}
			else if(typeof temp == "undefined"){
				if(!isNaN(parseFloat(heat_transfer_ratio))){
					if(!isNaN(parseFloat(heat_transfer_coeff))){
						if(!isNaN(parseFloat(eq_temp))){
							if(!isNaN(parseFloat(area))){
								calcula_temp(heat_transfer_coeff,heat_transfer_ratio,eq_temp,area);
							}
							else{
							alert("El área debe ser un número");
							}
						}
						else{
							alert("La temperatura de equilibrio debe ser un número");
						}
					}
					else{
						alert("El coeficiente de transferencia de calor debe ser un número");
					}
				}
				else{
					alert("La tasa de transferencia de calor debe ser un número");
				}
			}
			else if(typeof eq_temp == "undefined"){
				if(!isNaN(parseFloat(heat_transfer_ratio))){
					if(!isNaN(parseFloat(heat_transfer_coeff))){
						if(!isNaN(parseFloat(temp))){
							if(!isNaN(parseFloat(area))){
								calcula_temp_eq(heat_transfer_coeff,temp,heat_transfer_ratio,area);
							}
							else{
							alert("El área debe ser un número");
							}
						}
						else{
							alert("La temperatura debe ser un número");
						}
					}
					else{
						alert("El coeficiente de transferencia de calor debe ser un número");
					}
				}
				else{
					alert("La tasa de transferencia de calor debe ser un número");
				}
			}
			else if(typeof area == "undefined"){
				if(!isNaN(parseFloat(heat_transfer_ratio))){
					if(!isNaN(parseFloat(heat_transfer_coeff))){
						if(!isNaN(parseFloat(temp))){
							if(!isNaN(parseFloat(eq_temp))){
								calcula_area(heat_transfer_coeff,temp,eq_temp,heat_transfer_ratio);
							}
							else{
							alert("La temperatura de equilibrio debe ser un número");
							}
						}
						else{
							alert("La temperatura debe ser un número");
						}
					}
					else{
						alert("El coeficiente de transferencia de calor debe ser un número");
					}
				}
				else{
					alert("La tasa de transferencia de calor debe ser un número");
				}
			}
		});

	};

	this.eventos_ = function(){
		eventos();
	};


	function fill_fields(var1,var2,var3,var4){
		var index_fields = [var1,var2,var3,var4];
		var html_fields = {
			"1": "<p>Tasa de transferencia de calor (dQ/dt, en Watts):</p><input type='number' class='heat_transfer_rate'>",
			"2": "<p>Coeficiente de transferencia de calor (h, en W/m2.K):</p><input type='number' class='heat_transfer_coefficient'>",
			"3": "<p>Temperatura (T, en °K):</p><input type='number' class='temperature'>",
			"4": "<p>Temperatura de equilibrio (T2, en °K):</p><input type='number' class='equilibrium_temperature'>",
			"5": "<p>Área (A, en m2):</p><input type='number' class='area'>"
		}
		var c = "";
		for (var i = 0; i < index_fields.length; i++) {
			c += html_fields[index_fields[i]];
		}
		c += "<br><button class='calculus_button'>Calcular</button>";
		$(".container_variables").html(c);
		eventos();
	}

	function calcula_htr(heat_transfer_coeff,temp,eq_temp,area){
		var htc = parseFloat(heat_transfer_coeff);
		var t = parseFloat(temp);
		var a = parseFloat(area);
		var e_t = parseFloat(eq_temp);
		var resultado = (htc*a)*(e_t-t);
		var c = "";
		c += "<p>Tasa de transferencia de calor:</p>";
		c += "<p>" + resultado + "Watt(s)</p>";
		$(".container_resupuesta").html(c);
	}

	function calcula_htc(heat_transfer_ratio,temp,eq_temp,area){
		var htr = parseFloat(heat_transfer_ratio);
		var t = parseFloat(temp);
		var a = parseFloat(area);
		var e_t = parseFloat(eq_temp);
		var resultado = htr/(a*(e_t-t));
		var c = "";
		c += "<p>Coeficiente de transferencia de calor:</p>";
		c += "<p>" + resultado + "W/m2.K</p>";
		$(".container_resupuesta").html(c);
	}

	function calcula_temp(heat_transfer_coeff,heat_transfer_ratio,eq_temp,area){
		var htr = parseFloat(heat_transfer_ratio);
		var htc = parseFloat(heat_transfer_coeff);
		var a = parseFloat(area);
		var e_t = parseFloat(eq_temp);
		var resultado = -((htr/(htc*a))-e_t);
		var c = "";
		c += "<p>Coeficiente de transferencia de calor:</p>";
		c += "<p>" + resultado + "°K</p>";
		$(".container_resupuesta").html(c);
	}

	function calcula_temp_eq(heat_transfer_coeff,temp,heat_transfer_ratio,area){
		var htr = parseFloat(heat_transfer_ratio);
		var htc = parseFloat(heat_transfer_coeff);
		var a = parseFloat(area);
		var t = parseFloat(temp);
		var resultado = ((htr/(htc*a))+t);
		var c = "";
		c += "<p>Coeficiente de transferencia de calor:</p>";
		c += "<p>" + resultado + "°K</p>";
		$(".container_resupuesta").html(c);
	}

	function calcula_area(heat_transfer_coeff,temp,eq_temp,heat_transfer_ratio){
		var htr = parseFloat(heat_transfer_ratio);
		var htc = parseFloat(heat_transfer_coeff);
		var e_t = parseFloat(eq_temp);
		var t = parseFloat(temp);
		var resultado = htc*(htr*(e_t-t));
		var c = "";
		c += "<p>Coeficiente de transferencia de calor:</p>";
		c += "<p>" + resultado + "m2</p>";
		$(".container_resupuesta").html(c);
	}
}