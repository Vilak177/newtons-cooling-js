# Calculador de múltiples variables de la ecuación de enfriamiento de Newton.

Este pequeño sistema calcula el coeficiente de transferencia de calor, la tasa de transferencia de calor, la temperatura de equilibrio, la temperatura de la superficie y el área de acuerdo a la información proporcionada

## Archivos.

El sistema requiere del archivo html principal "index.html" y los archivos JavaScript "newtons_cooling.js" y "jquery-3.2.1.min.js" en un carpeta llamada "js" con carpeta padre llamada "assets" la cual debe estar al mismo nivel que el archvio html.

## Funcionalidad.

Para iniciar el sistema se abre el archivo html con un navegador con JavaScript habilitado y se selecciona del menú de selección la variable que se desea obtener, en base a la selección se mostrarán los campos que se deben llenar para obtener un resultado, una vez que estos sean llenados se debe presionar el botón "Calcular", el cual validará que la información ingresada sea correcta, de no ser así el sistema mostrará un mensaje de error indicando en que campo se encuentra el error.
Si la información es correcta el sistema realizará el cálculo adecuado y mostrará el resultado debajo de los campos y el botón calcular.

